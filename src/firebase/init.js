import firebase from 'firebase'
import firestore from 'firebase/firestore'

// Initialize Firebase
var config = {
    apiKey: "AIzaSyDIz11pkpkpo8QQRJpn8zOn7vJZx73m1GM",
    authDomain: "udemy-geo-ninjas-2019.firebaseapp.com",
    databaseURL: "https://udemy-geo-ninjas-2019.firebaseio.com",
    projectId: "udemy-geo-ninjas-2019",
    storageBucket: "",
    messagingSenderId: "589706196489"
  };

const firebaseApp = firebase.initializeApp(config)

firebaseApp.firestore().settings( {timestampsInSnapshots: true})

export default firebaseApp.firestore()